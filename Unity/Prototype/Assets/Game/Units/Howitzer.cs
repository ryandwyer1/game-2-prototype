﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class Howitzer : Unit
{
    public override Specialization specialization { get; set; }
    public override Specialization toAttack { get; set; }
    public override bool hasUpdatedBuffs { get; set; }
    public override bool hasAttacked { get; set; }
    public override void Start()
    {
        base.Start();
        specialization = Specialization.Artillery;
        level = 1;
        tier = 3;
        range = 5;
        unitName = "Howitzer";
        UpdateLevel(level);
    }
    
    public override void Update()
    {
        base.Update();
        if (!hasUpdatedBuffs && spawned)
        {
            hasUpdatedBuffs = true;
            GameManager.instance.gameObject.GetPhotonView().RPC("UpdateOwnerSpecializations", RpcTarget.All);
        }
    }
    
    //TODO: Update stats per spreadsheet
    public void UpdateLevel(int newLevel)
    {
        level = newLevel;
        switch (level)
        {
            case 1:
                health = 550f;
                armor = 10f;
                damage = 65f;
                attackSpeed = 1.5f;
                cost = 100f;
                break;
            case 2:
                health = 1100f;
                armor = 10f;
                damage = 130f;
                attackSpeed = 1.5f;
                cost = 150f;
                break;
            case 3:
                health = 2200f;
                armor = 10f;
                damage = 240f;
                attackSpeed = 0.7f;
                cost = 260f;
                break;
            default:
                Debug.Log("Ryan fucked up!");
                break;
        }

        maxHealth = health;
    }
}