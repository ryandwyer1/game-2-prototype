﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class Autorifleman : Unit
{
    public override Specialization specialization { get; set; }
    public override Specialization toAttack { get; set; }
    public override bool hasUpdatedBuffs { get; set; }
    public override bool hasAttacked { get; set; }
    public override void Start()
    {
        base.Start();
        specialization = Specialization.Rifleman;
        tier = level = 1;
        range = 2;
        unitName = "Autorifleman";
        UpdateLevel(level);
    }
    
    public override void Update()
    {
        base.Update();
        if (!hasUpdatedBuffs && spawned)
        {
            hasUpdatedBuffs = true;
            GameManager.instance.gameObject.GetPhotonView().RPC("UpdateOwnerSpecializations", RpcTarget.All);
        }
    }
    
    public void UpdateLevel(int newLevel)
    {
        level = newLevel;
        switch (level)
        {
            case 1:
                health = 450f;
                armor = 5f;
                damage = 50f;
                attackSpeed = 1.1f;
                cost = 100f;
                break;
            case 2:
                health = 900f;
                armor = 5f;
                damage = 100f;
                attackSpeed = 0.9f;
                cost = 150f;
                break;
            case 3:
                health = 1800f;
                armor = 5f;
                damage = 200f;
                attackSpeed = 0.7f;
                cost = 250f;
                break;
            default:
                Debug.LogError("Ryan fucked up!");
                break;
        }

        maxHealth = health;
    }
}
