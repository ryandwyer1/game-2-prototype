﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class M60A3 : Unit
{
    public override Specialization specialization { get; set; }
    public override Specialization toAttack { get; set; }
    public override bool hasUpdatedBuffs { get; set; }
    public override bool hasAttacked { get; set; }
    public override void Start()
    {
        base.Start();
        specialization = Specialization.Armor;
        tier = range = 2;
        level = 1;
        UpdateLevel(level);
        unitName = "M60A3";
    }
    
    public override void Update()
    {
        base.Update();
        if (!hasUpdatedBuffs && spawned)
        {
            hasUpdatedBuffs = true;
            GameManager.instance.gameObject.GetPhotonView().RPC("UpdateOwnerSpecializations", RpcTarget.All);
        }
    }
    
    //TODO: Update stats per spreadsheet
    public void UpdateLevel(int newLevel)
    {
        level = newLevel;
        switch (level)
        {
            case 1:
                health = 900f;
                armor = 10f;
                damage = 50f;
                attackSpeed = 1.3f;
                cost = 100f;
                break;
            case 2:
                health = 1300f;
                armor = 10f;
                damage = 100f;
                attackSpeed = 1.3f;
                cost = 150f;
                break;
            case 3:
                health = 2000f;
                armor = 10f;
                damage = 200f;
                attackSpeed = 1.3f;
                cost = 250f;
                break;
            default:
                Debug.Log("Ryan fucked up!");
                break;
        }

        maxHealth = health;
    }
}