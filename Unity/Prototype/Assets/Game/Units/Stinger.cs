﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class Stinger : Unit
{
    public override Specialization specialization { get; set; }
    public override Specialization toAttack { get; set; }
    public override bool hasUpdatedBuffs { get; set; }
    public override bool hasAttacked { get; set; }
    public override void Start()
    {
        base.Start();
        specialization = Specialization.AntiAir;
        tier = 2;
        range = 3;
        level = 1;
        UpdateLevel(level);
        unitName = "Stinger";
    }
    
    public override void Update()
    {
        base.Update();
        if (!hasUpdatedBuffs && spawned)
        {
            hasUpdatedBuffs = true;
            GameManager.instance.gameObject.GetPhotonView().RPC("UpdateOwnerSpecializations", RpcTarget.All);
        }
    }
    
    //TODO: Update stats per spreadsheet
    public void UpdateLevel(int newLevel)
    {
        level = newLevel;
        switch (level)
        {
            case 1:
                health = 600f;
                armor = 10f;
                damage = 70f;
                attackSpeed = 1.3f;
                cost = 100f;
                break;
            case 2:
                health = 1200f;
                armor = 10f;
                damage = 140f;
                attackSpeed = 1.3f;
                cost = 150f;
                break;
            case 3:
                health = 2400f;
                armor = 10f;
                damage = 280f;
                attackSpeed = 1.3f;
                cost = 250f;
                break;
            default:
                Debug.Log("Ryan fucked up!");
                break;
        }

        maxHealth = health;
    }
}