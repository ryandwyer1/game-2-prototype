﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class ZoneScout : Unit
{
    public override Specialization specialization { get; set; }
    public override Specialization toAttack { get; set; }
    public override bool hasUpdatedBuffs { get; set; }
    public override bool hasAttacked { get; set; }
    public override void Start()
    {
        base.Start();
        specialization = Specialization.Recon;
        tier = 3;
        range = 10;
        level = 1;
        UpdateLevel(level);
        unitName = "Zone Scout";
    }
    
    public override void Update()
    {
        base.Update();
        if (!hasUpdatedBuffs && spawned)
        {
            hasUpdatedBuffs = true;
            GameManager.instance.gameObject.GetPhotonView().RPC("UpdateOwnerSpecializations", RpcTarget.AllViaServer);
        }
    }
    
    //TODO: Update stats per spreadsheet
    public void UpdateLevel(int newLevel)
    {
        level = newLevel;
        switch (level)
        {
            case 1:
                health = 550f;
                armor = 5f;
                damage = 70f;
                attackSpeed = 1.3f;
                cost = 100f;
                break;
            case 2:
                health = 1100f;
                armor = 5f;
                damage = 140f;
                attackSpeed = 1.3f;
                cost = 150f;
                break;
            case 3:
                health = 2200f;
                armor = 5f;
                damage = 280f;
                attackSpeed = 1.3f;
                cost = 250f;
                break;
            default:
                Debug.Log("Ryan fucked up!");
                break;
        }

        maxHealth = health;
    }
}