﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class Sniper : Unit
{
    public override Specialization specialization { get; set; }
    public override Specialization toAttack { get; set; }
    public override bool hasUpdatedBuffs { get; set; }
    public override bool hasAttacked { get; set; }
    public override void Start()
    {
        base.Start();
        specialization = Specialization.Rifleman;
        tier = 2;
        range = 6;
        level = 1;
        UpdateLevel(level);
        unitName = "Sniper";
    }
    
    public override void Update()
    {
        base.Update();
        if (!hasUpdatedBuffs && spawned)
        {
            hasUpdatedBuffs = true;
            GameManager.instance.gameObject.GetPhotonView().RPC("UpdateOwnerSpecializations", RpcTarget.All);
        }
    }
    
    public void UpdateLevel(int newLevel)
    {
        level = newLevel;
        switch (level)
        {
            case 1:
                health = 450f;
                armor = 0f;
                damage = 75f;
                attackSpeed = 1.5f;
                cost = 100f;
                break;
            case 2:
                health = 900f;
                armor = 0f;
                damage = 150f;
                attackSpeed = 1.5f;
                cost = 150f;
                break;
            case 3:
                health = 1800f;
                armor = 0f;
                damage = 250f;
                attackSpeed = 1.5f;
                cost = 250f;
                break;
            default:
                Debug.Log("Ryan fucked up!");
                break;
        }

        maxHealth = health;
    }
}