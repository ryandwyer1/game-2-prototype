﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class Sapper : Unit
{
    public override Specialization specialization { get; set; }
    public override Specialization toAttack { get; set; }
    public override bool hasUpdatedBuffs { get; set; }
    public override bool hasAttacked { get; set; }
    public override void Start()
    {
        base.Start();
        specialization = Specialization.Engineer;
        tier = 2;
        range = level = 1;
        UpdateLevel(level);
        unitName = "Sapper";
    }
    
    public override void Update()
    {
        base.Update();
        if (!hasUpdatedBuffs && spawned)
        {
            hasUpdatedBuffs = true;
            GameManager.instance.gameObject.GetPhotonView().RPC("UpdateOwnerSpecializations", RpcTarget.All);
        }
    }
    
    //TODO: Update stats per spreadsheet
    public void UpdateLevel(int newLevel)
    {
        level = newLevel;
        switch (level)
        {
            case 1:
                health = 700f;
                armor = 5f;
                damage = 40f;
                attackSpeed = 1.1f;
                cost = 100f;
                break;
            case 2:
                health = 1050f;
                armor = 7f;
                damage = 80f;
                attackSpeed = 1.1f;
                cost = 150f;
                break;
            case 3:
                health = 1575f;
                armor = 10f;
                damage = 160f;
                attackSpeed = 1.1f;
                cost = 250f;
                break;
            default:
                Debug.Log("Ryan fucked up!");
                break;
        }

        maxHealth = health;
    }
}