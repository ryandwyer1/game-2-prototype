﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Ambulance : Unit
{
    public override Specialization specialization { get; set; }
    public override Specialization toAttack { get; set; }
    public override bool hasUpdatedBuffs { get; set; }
    public override bool hasAttacked { get; set; }
    public override void Start()
    {
        base.Start();
        specialization = Specialization.Medic;
        unitName = "Ambulance";
        tier = 3;
        range = 0;
        level = 1;
        UpdateLevel(level);
    }
    
    public override void Update()
    {
        base.Update();
        if (!hasUpdatedBuffs && spawned)
        {
            hasUpdatedBuffs = true;
            GameManager.instance.gameObject.GetPhotonView().RPC("UpdateOwnerSpecializations", RpcTarget.All);
        }
    }
    
    //TODO: Update stats per spreadsheet
    public void UpdateLevel(int newLevel)
    {
        level = newLevel;
        switch (level)
        {
            case 1:
                health = 800f;
                armor = 10f;
                damage = 0f;
                attackSpeed = 1.0f;
                cost = 100f;
                break;
            case 2:
                health = 1600f;
                armor = 10f;
                damage = 0f;
                attackSpeed = 1.0f;
                cost = 150f;
                break;
            case 3:
                health = 3200f;
                armor = 10f;
                damage = 0f;
                attackSpeed = 1.0f;
                cost = 250f;
                break;
            default:
                Debug.Log("Ryan fucked up!");
                break;
        }

        maxHealth = health;
    }
}