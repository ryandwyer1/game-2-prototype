﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class RouteScout : Unit
{
    public override Specialization specialization { get; set; }
    public override Specialization toAttack { get; set; }
    public override bool hasUpdatedBuffs { get; set; }
    public override bool hasAttacked { get; set; }
    public override void Start()
    {
        base.Start();
        specialization = Specialization.Recon;
        tier = level = 1;
        range = 5;
        unitName = "Route Scout";
        UpdateLevel(level);
    }
    public override void Update()
    {
        base.Update();
        if (!hasUpdatedBuffs && spawned)
        {
            hasUpdatedBuffs = true;
            GameManager.instance.gameObject.GetPhotonView().RPC("UpdateOwnerSpecializations", RpcTarget.All);
        }
    }
    
    
    //TODO: Update stats per spreadsheet
    public void UpdateLevel(int newLevel)
    {
        level = newLevel;
        switch (level)
        {
            case 1:
                health = 600f;
                armor = 10f;
                damage = 55f;
                attackSpeed = 1.5f;
                cost = 100f;
                break;
            case 2:
                health = 1200f;
                armor = 10f;
                damage = 110f;
                attackSpeed = 1.5f;
                cost = 150f;
                break;
            case 3:
                health = 2400f;
                armor = 10f;
                damage = 220f;
                attackSpeed = 1.5f;
                cost = 250f;
                break;
            default:
                Debug.Log("Ryan fucked up!");
                break;
        }

        maxHealth = health;
    }
}