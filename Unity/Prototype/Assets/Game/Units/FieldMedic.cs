﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class FieldMedic : Unit
{
    public override Specialization specialization { get; set; }
    public override Specialization toAttack { get; set; }
    public override bool hasUpdatedBuffs { get; set; }
    public override bool hasAttacked { get; set; }
    public override void Start()
    {
        base.Start();
        specialization = Specialization.Medic;
        tier = range = level = 1;
        unitName = "Field Medic";
        UpdateLevel(level);
    }

    public override void Update()
    {
        base.Update();
        if (!hasUpdatedBuffs && spawned)
        {
            hasUpdatedBuffs = true;
            GameManager.instance.gameObject.GetPhotonView().RPC("UpdateOwnerSpecializations", RpcTarget.All);
        }
    }

    public void UpdateLevel(int newLevel)
    {
        level = newLevel;
        switch (level)
        {
            case 1:
                health = 600f;
                armor = 5f;
                damage = 45f;
                attackSpeed = 1.5f;
                cost = 100f;
                break;
            case 2:
                health = 1200f;
                armor = 5f;
                damage = 70f;
                attackSpeed = 1.5f;
                cost = 150f;
                break;
            case 3:
                health = 2200f;
                armor = 5f;
                damage = 100f;
                attackSpeed = 1.5f;
                cost = 250f;
                break;
            default:
                Debug.Log("Ryan fucked up!");
                break;
        }

        maxHealth = health;
    }
}