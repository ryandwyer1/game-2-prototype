﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class A10Warthog : Unit
{
    public override Specialization specialization { get; set; }
    public override Specialization toAttack { get; set; }
    public override bool hasUpdatedBuffs { get; set; }
    public override bool hasAttacked { get; set; }
    public override void Start()
    {
        base.Start();
        specialization = Specialization.Air;
        unitName = "A10Warthog";
        tier = 2;
        range = 3;
        level = 1;
        UpdateLevel(level);
    }
    
    public override void Update()
    {
        base.Update();
        if (!hasUpdatedBuffs && spawned)
        {
            Debug.Log("Updating");
            hasUpdatedBuffs = true;
            GameManager.instance.gameObject.GetPhotonView().RPC("UpdateOwnerSpecializations", RpcTarget.All);
        }
    }
    
    //TODO: Update stats per spreadsheet
    public void UpdateLevel(int newLevel)
    {
        level = newLevel;
        switch (level)
        {
            case 1:
                health = 700f;
                maxHealth = 700f;
                armor = 5f;
                damage = 65f;
                attackSpeed = 1.5f;
                cost = 100f;
                break;
            case 2:
                health = 1400f;
                maxHealth = 1400f;
                armor = 5f;
                damage = 130f;
                attackSpeed = 1.5f;
                cost = 150f;
                break;
            case 3:
                health = 2800f;
                armor = 5f;
                damage = 240f;
                attackSpeed = 1.5f;
                cost = 250f;
                break;
            default:
                Debug.Log("Ryan fucked up!");
                break;
        }

        maxHealth = health;
    }
}