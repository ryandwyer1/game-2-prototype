﻿using Photon.Pun;
using UnityEngine;

public class Grunt : Unit
{
    public override Specialization specialization { get; set; }
    public override Specialization toAttack { get; set; }
    public override bool hasUpdatedBuffs { get; set; }
    public override bool hasAttacked { get; set; }
    public override void Start()
    {
        base.Start();
        specialization = Specialization.Rifleman;
        tier = range = level = 1;
        unitName = "Grunt";
        UpdateLevel(level);
    }

    public override void Update()
    {
        base.Update();
        if (!hasUpdatedBuffs && spawned)
        {
            Debug.Log("Spawned");
            hasUpdatedBuffs = true;
            GameManager.instance.gameObject.GetPhotonView().RPC("UpdateOwnerSpecializations", RpcTarget.All);
        }
    }
    
    public void UpdateLevel(int newLevel)
    {
        level = newLevel;
        switch (level)
        {
            case 1:
                health = 500f;
                armor = 5f;
                damage = 45f;
                attackSpeed = 1.3f;
                cost = 100f;
                break;
            case 2:
                health = 1000f;
                armor = 5f;
                damage = 90f;
                attackSpeed = 1.3f;
                cost = 150f;
                break;
            case 3:
                health = 2000f;
                armor = 5f;
                damage = 180f;
                attackSpeed = 1.3f;
                cost = 250f;
                break;
            default:
                Debug.Log("Ryan fucked up!");
                break;
        }

        maxHealth = health;
    }
}
