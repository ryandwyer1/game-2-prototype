﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class AC130 : Unit
{
    public override Specialization specialization { get; set; }
    public override Specialization toAttack { get; set; }
    public override bool hasUpdatedBuffs { get; set; }
    public override bool hasAttacked { get; set; }
    public override void Start()
    {
        base.Start();
        specialization = Specialization.Air;
        unitName = "AC130";
        tier = 3;
        range = 4;
        level = 1;
        UpdateLevel(level);
    }
    
    public override void Update()
    {
        base.Update();
        if (!hasUpdatedBuffs && spawned)
        {
            hasUpdatedBuffs = true;
            GameManager.instance.gameObject.GetPhotonView().RPC("UpdateOwnerSpecializations", RpcTarget.All);       
        }
    }
    
    //TODO: Update stats per spreadsheet
    public void UpdateLevel(int newLevel)
    {
        level = newLevel;
        switch (level)
        {
            case 1:
                health = 700f;
                armor = 10f;
                damage = 65f;
                attackSpeed = 1.5f;
                cost = 100f;
                break;
            case 2:
                health = 1300f;
                armor = 10f;
                damage = 130f;
                attackSpeed = 1.5f;
                cost = 150f;
                break;
            case 3:
                health = 2600f;
                armor = 10f;
                damage = 260f;
                attackSpeed = 1.5f;
                cost = 250f;
                break;
            default:
                Debug.Log("Ryan fucked up!");
                break;
        }

        maxHealth = health;
    }
}