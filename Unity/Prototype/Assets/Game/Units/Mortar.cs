﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class Mortar : Unit
{
    public override Specialization specialization { get; set; }
    public override Specialization toAttack { get; set; }
    public override bool hasUpdatedBuffs { get; set; }
    public override bool hasAttacked { get; set; }
    public override void Start()
    {
        base.Start();
        specialization = Specialization.Artillery;
        tier = 2;
        range = 5;
        level = 1;
        UpdateLevel(level);
        unitName = "Mortar";
    }
    
    public override void Update()
    {
        base.Update();
        if (!hasUpdatedBuffs && spawned)
        {
            hasUpdatedBuffs = true;
            GameManager.instance.gameObject.GetPhotonView().RPC("UpdateOwnerSpecializations", RpcTarget.All);
        }
    }
    
    //TODO: Update stats per spreadsheet
    public void UpdateLevel(int newLevel)
    {
        level = newLevel;
        switch (level)
        {
            case 1:
                health = 500f;
                armor = 5f;
                damage = 60f;
                attackSpeed = 1.3f;
                cost = 100f;
                break;
            case 2:
                health = 1000f;
                armor = 5f;
                damage = 120f;
                attackSpeed = 1.3f;
                cost = 150f;
                break;
            case 3:
                health = 2000f;
                armor = 5f;
                damage = 200f;
                attackSpeed = 1.3f;
                cost = 250f;
                break;
            default:
                Debug.Log("Ryan fucked up!");
                break;
        }

        maxHealth = health;
    }
}