﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class TOWTeam : Unit
{
    public override Specialization specialization { get; set; }
    public override Specialization toAttack { get; set; }
    public override bool hasUpdatedBuffs { get; set; }
    public override bool hasAttacked { get; set; }
    public override void Start()
    {
        base.Start();
        specialization = Specialization.AntiTank;
        tier = 3;
        range = 5;
        level = 1;
        UpdateLevel(level);
        unitName = "TOW Team";
    }
    
    public override void Update()
    {
        base.Update();
        if (!hasUpdatedBuffs && spawned)
        {
            hasUpdatedBuffs = true;
            GameManager.instance.gameObject.GetPhotonView().RPC("UpdateOwnerSpecializations", RpcTarget.All);
        }
    }
    
    //TODO: Update stats per spreadsheet
    public void UpdateLevel(int newLevel)
    {
        level = newLevel;
        switch (level)
        {
            case 1:
                health = 650f;
                armor = 5f;
                damage = 80f;
                attackSpeed = 1.3f;
                cost = 100f;
                break;
            case 2:
                health = 1300f;
                armor = 5f;
                damage = 160f;
                attackSpeed = 1.3f;
                cost = 150f;
                break;
            case 3:
                health = 2600f;
                armor = 5f;
                damage = 320f;
                attackSpeed = 1.3f;
                cost = 250f;
                break;
            default:
                Debug.Log("Ryan fucked up!");
                break;
        }

        maxHealth = health;
    }
}