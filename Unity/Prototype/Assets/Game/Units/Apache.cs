﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Apache : Unit
{
    public override Specialization specialization { get; set; }
    public override Specialization toAttack { get; set; }
    public override bool hasUpdatedBuffs { get; set; }
    public override bool hasAttacked { get; set; }
    public override void Start()
    {
        base.Start();
        specialization = Specialization.Air;
        unitName = "Apache";
        tier = level = 1;
        range = 2;
        UpdateLevel(level);
    }
    
    public override void Update()
    {
        base.Update();
        if (!hasUpdatedBuffs && spawned)
        {
            Debug.Log("Spawned");
            hasUpdatedBuffs = true;
            GameManager.instance.gameObject.GetPhotonView().RPC("UpdateOwnerSpecializations", RpcTarget.All);
        }
    }
    
    //TODO: Update stats per spreadsheet
    public void UpdateLevel(int newLevel)
    {
        level = newLevel;
        switch (level)
        {
            case 1:
                health = 600f;
                armor = 5f;
                damage = 60f;
                attackSpeed = 1.1f;
                cost = 100f;
                break;
            case 2:
                health = 1200f;
                armor = 5f;
                damage = 120f;
                attackSpeed = 1.1f;
                cost = 150f;
                break;
            case 3:
                health = 2400f;
                armor = 5f;
                damage = 240f;
                attackSpeed = 1.0f;
                cost = 250f;
                break;
            default:
                Debug.Log("Ryan fucked up!");
                break;
        }

        maxHealth = health;
    }
}