﻿using UnityEngine;
using System.Collections.Generic;

public enum Specialization 
{
    Rifleman = 0,
    AntiTank = 1,
    Medic = 2,
    Recon = 3,
    MechanizedInfantry = 4,
    Artillery = 5,
    Armor = 6,
    Air = 7,
    AntiAir = 8,
    Engineer = 9,
    Empty = 10
}

public class SpecializationManager
{
    public static SpecializationManager instance;
    
    private float damageDelta, healthDelta, armorDelta, speedDelta;
    public bool rifleman, antiTank, medic, recon, artillery, engineer;

    public SpecializationManager()
    {
        instance = this;
        damageDelta = healthDelta = armorDelta = speedDelta = 0;
        rifleman = antiTank = medic = recon = artillery = engineer = false;
    }

    public void ChangeStats()
    {
        damageDelta = healthDelta = armorDelta = speedDelta = 0;
        if (rifleman)
            damageDelta += 10f;
        if (medic)
            healthDelta += 100f;
        if (artillery)
            speedDelta += 0.3f;
        if (artillery)
            armorDelta += 5f;

        foreach (GameObject unitObj in Unit.GetFriendlies())
        {
            Unit unit = unitObj.GetComponent<Unit>();
            unit.damage += damageDelta;
            unit.maxHealth += healthDelta;
            unit.health += healthDelta;
            unit.attackSpeed -= speedDelta;
        }
    }

    public void ActivateBuff(Specialization toActivate)
    {
        switch (toActivate)
        {
            case Specialization.Rifleman:
                rifleman = true;
                break;
            case Specialization.Artillery:
                artillery = true;
                break;
            case Specialization.Engineer:
                engineer = true;
                break;
            case Specialization.Medic:
                medic = true;
                break;
            case Specialization.Recon:
                recon = true;
                break;
            case Specialization.AntiTank:
                antiTank = true;
                break;
        }
        ChangeStats();
    }
}

//3 rifleman = 10 damage boost
//2 anti-tank: 5 armor damage boost
//2 Medic: Every unit gets 100 more HP
//2 Recon: TBD
//2 Artillery: Increases attack rate by 10
//2 Engineer: Increase base armor of all units by 5