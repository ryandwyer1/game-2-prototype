﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour
{
    public static Tooltip instance;
    
    [SerializeField] private Text titleText, statsText;
    private readonly Vector3 displayOffset = new Vector3(-100f, 45f, -5f);
    void Awake()
    {
        instance = this;
        gameObject.SetActive(false);
    }
    void Update()
    {
        transform.position = Input.mousePosition + displayOffset;
    }

    public void ToggleState(bool newState)
    {
        transform.position = Input.mousePosition + displayOffset;
        gameObject.SetActive(newState);
    }

    public void SetUnitInfo(string unitName, float cost, float health, float armor, float range, float level, float tier)
    {
        transform.position = Input.mousePosition + displayOffset;
        titleText.text = unitName;
        statsText.text = $"Cost: {cost}\nHealth: {health}\nArmor: {armor}\nRange: {range}\nLevel: {level}\nTier: {tier}";
    }

    public void ResetUnitInfo()
    {
        transform.position = Input.mousePosition + displayOffset;
        titleText.text = "";
        statsText.text = "";
    }
}
