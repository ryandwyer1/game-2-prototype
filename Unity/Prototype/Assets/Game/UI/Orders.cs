﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class Orders : MonoBehaviour
{
    public static Orders instance;
    public List<RectTransform> specializationPanels;
    public List<Dropdown> orderLists;

    #region Unity Event Functions
    void Start()
    {
        instance = this;
        foreach (RectTransform panel in specializationPanels)
        {
            panel.gameObject.SetActive(false);
        }
        AssignListeners();
    }
    #endregion

    #region Public Methods
    public void UpdatePanels()
    {
        for (int i = 0; i < Player.localPlayerInstance.specializations.Length; i++)
        {
            //Debug.Log(i + " " + Player.localPlayerInstance.specializations[i]);
            specializationPanels[i].gameObject.SetActive(Player.localPlayerInstance.specializations[i] > 0);
        }
    }

    public bool[] GetSpecializations(int playerNum)
    {
        if (PhotonNetwork.PlayerList.Length < 2)
        {
            Debug.LogError("Tried to order units before match starts!");
            throw new UnauthorizedAccessException();
        }
        
        bool[] activeSpecializations = new bool[10];
        Player target = Player.localPlayerInstance;
        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (player.GetComponent<Player>().playerNum == playerNum)
                target = player.GetComponent<Player>();
        }

        if (target.playerNum != playerNum)
            GetSpecializations(playerNum);
        
        for (int i = 0; i < 10; i++)
        {
            if (target.specializations[i] > 0)
                activeSpecializations[i] = true;
        }

        return activeSpecializations;
    }

    public void GetEnemyTargets()
    {
        foreach (Dropdown targetList in orderLists)
        {
            targetList.options.Clear();
        }
        
        if (PhotonNetwork.PlayerList.Length < 2)
            return;

        int enemyPlayer;
        if (Player.localPlayerInstance.playerNum == 1)
            enemyPlayer = 2;
        else
            enemyPlayer = 1;
        
        bool[] potentialTargets = GetSpecializations(enemyPlayer);
        
        foreach (Dropdown targetList in orderLists)
        {
            targetList.options.Add(new Dropdown.OptionData(((Specialization)10).ToString()));
        }
        
        for (int i = 0; i < potentialTargets.Length; i++)
        {
            if (potentialTargets[i])
            {
                foreach (Dropdown targetList in orderLists)
                {
                    targetList.options.Add(new Dropdown.OptionData(((Specialization)i).ToString()));
                    targetList.RefreshShownValue();
                }
                
            }
        }
    }
    #endregion
    
    #region Private Methods

    private void AssignListeners()
    {
        foreach (Dropdown dropdown in orderLists)
        {
            dropdown.onValueChanged.AddListener(delegate {AssignOrders(dropdown);});
        }
    }

    private void AssignOrders(Dropdown orderList)
    {
        Specialization toTarget = (Specialization) orderList.value;
        Specialization toOrder = (Specialization) orderLists.IndexOf(orderList);

        if (orderList.options[orderList.value].text == "Empty")
            toTarget = Specialization.Empty;

        List<GameObject> myUnits;
        if (Player.localPlayerInstance.playerNum == 1)
            myUnits = GlobalVars.shared.playerOneUnits;
        else
            myUnits = GlobalVars.shared.playerTwoUnits;

        foreach (GameObject unit in myUnits)
        {
            Unit unitScript = unit.GetComponent<Unit>();
            if (toOrder == unitScript.specialization)
            {
                unitScript.toAttack = toTarget;
            }
        }
    }
    
    #endregion
}
