﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

public class BunkerCard : MonoBehaviourPunCallbacks, IDragHandler, IEndDragHandler
{
    public Vector3 bunkerPos;

    private Ray spawnRay;
    private string[] cardName;
    
    void Start()
    {
        cardName = gameObject.name.Split('_');
    }
    
    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (Physics.Raycast(Camera.main.ScreenPointToRay(transform.position), out RaycastHit hit))
        {
            if((Player.localPlayerInstance.playerNum == 1 && hit.transform.CompareTag("PlayerOneTile")) || 
               (Player.localPlayerInstance.playerNum == 2 && hit.transform.CompareTag("PlayerTwoTile")))
            {
                if (!hit.transform.GetComponent<Tile>().occupied)
                {
                    List<GameObject> units = GlobalVars.shared.unitPrefabs;
                    foreach (GameObject prefab in units)
                    {
                        string[] unitName = prefab.name.Split('_');
                        if (unitName[0] != cardName[0]) continue;
                        GameObject unit = PhotonNetwork.Instantiate(prefab.name, hit.transform.position, Quaternion.identity);
                        GameManager.instance.gameObject.GetPhotonView().RPC("SpawnUnit", RpcTarget.AllViaServer, hit.transform.position, unit.GetPhotonView().ViewID);
                        hit.transform.GetComponent<Tile>().occupied = true;
                        PhotonNetwork.Destroy(gameObject);
                    }
                }
                else
                {
                    RectTransform rect = transform as RectTransform;
                    rect.anchoredPosition = bunkerPos;
                }
            }
            else
            {
                RectTransform rect = transform as RectTransform;
                rect.anchoredPosition = bunkerPos;
            }
        }
        else
        {
            RectTransform rect = transform as RectTransform;
            rect.anchoredPosition = bunkerPos;
        }
    }
}