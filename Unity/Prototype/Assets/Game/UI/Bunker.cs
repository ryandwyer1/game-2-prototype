﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bunker : MonoBehaviour
{
    private bool extended;
    private Animation anim;

    void Start()
    {
        anim = gameObject.GetComponent<Animation>();
        anim.Play("BunkerSlideOut");
        extended = true;
    }

    public void UpdateCardPlacements()
    {
        float xPos = -645f;
        foreach (Transform child in transform)
        {
            if (!child.gameObject.CompareTag("Button"))
            {
                child.GetComponent<RectTransform>().anchoredPosition = new Vector2(xPos, 0);
                child.GetComponent<BunkerCard>().bunkerPos = new Vector3(xPos, 0f, 0f);
                xPos += 185;
            }
        }
    }

    public void ToggleAnim()
    {
        if (extended)
            anim.Play("BunkerSlideIn");
        else
            anim.Play("BunkerSlideOut");
        extended = !extended;
    }
}