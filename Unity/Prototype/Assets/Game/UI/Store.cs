﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Store : MonoBehaviour
{
    private bool extended;
    private Animation anim;

    void Start()
    {
        anim = gameObject.GetComponent<Animation>();
    }

    public void ToggleAnim()
    {
        if (extended)
            anim.Play("StoreSlideIn");
        else
            anim.Play("StoreSlideOut");
        extended = !extended;
    }
}
