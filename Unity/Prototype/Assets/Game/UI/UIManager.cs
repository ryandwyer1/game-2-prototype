﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIManager : MonoBehaviour
{
    public EventSystem eventSystem;

    [HideInInspector] public GraphicRaycaster raycaster;
    [HideInInspector] public PointerEventData tooltipEventData;
    
    void Start()
    {
        raycaster = GetComponent<GraphicRaycaster>();
        tooltipEventData = new PointerEventData(eventSystem);
    }

    void Update()
    {
        RaycastTooltip();
    }
    
    #region Private Methods

    private void RaycastTooltip()
    {
        tooltipEventData.position = Input.mousePosition;

        List<RaycastResult> results = new List<RaycastResult>();
        raycaster.Raycast(tooltipEventData, results);
        bool hasCard = false;
        foreach (RaycastResult hit in results)
        {
            if (hit.gameObject.CompareTag("Card"))
                hasCard = true;
        }

        if (hasCard)
        {
            foreach (RaycastResult hit in results)
            {
                Unit unit = hit.gameObject.GetComponent<Unit>();
                if (unit != null)
                {
                    Tooltip.instance.SetUnitInfo(unit.unitName, unit.cost, unit.health, unit.armor, unit.range,
                        unit.level, unit.tier);
                    Tooltip.instance.ToggleState(true);
                }
            }
        }
        else
        {
            Tooltip.instance.ResetUnitInfo();
            Tooltip.instance.ToggleState(false);
        }
    }
    
    #endregion
}
