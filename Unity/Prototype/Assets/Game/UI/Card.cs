﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Card : MonoBehaviour, IPointerUpHandler
{
    public Bunker bunker;
    
    private Text unitNameTip, unitStatsTip;
    private EventTrigger trigger;
    private string[] cardName;
    private int price;
    void Start()
    {
        cardName = gameObject.name.Split('_');
        price = GlobalVars.shared.GetTier(cardName[0]) * 100;
        bunker = GameObject.Find("Canvas/Bunker").GetComponent<Bunker>();
        
        
        trigger = gameObject.AddComponent<EventTrigger>();
        EventTrigger.Entry onClick = new EventTrigger.Entry();
        onClick.eventID = EventTriggerType.PointerDown;
        onClick.callback.AddListener(eventData => {OnPointerUp((PointerEventData)eventData);});
        trigger.triggers.Add(onClick);
    }
    
    public void OnPointerUp(PointerEventData eventData)
    {
        List<GameObject> prefabs = GlobalVars.shared.unitPrefabs;
        foreach (GameObject unit in prefabs)
        {
            string[] unitName = unit.name.Split('_');
            if (cardName[0] == unitName[0])
            {
                if (Player.localPlayerInstance.money >= price)
                {
                    Player.localPlayerInstance.money -= price;
                    MoveToBunker();
                }
            }
        }
    }

    private void MoveToBunker()
    {
        GameObject.Find("Canvas/Player Stats/Money").GetComponent<Text>().text =
            $"Money: {Player.localPlayerInstance.money}";
        GlobalVars.shared.instantiatedHand.Remove(gameObject);
        
        transform.SetParent(GameObject.Find("Canvas/Bunker").transform);
        gameObject.AddComponent<BunkerCard>();
        bunker.UpdateCardPlacements();
        
        Destroy(this);
    }
}
