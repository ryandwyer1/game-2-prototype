﻿using System;
using System.Collections.Generic;
using AStar_2D.Demo;
using UnityEngine;
using UnityEngine.EventSystems;
using Photon.Pun;
using Random = UnityEngine.Random;

public abstract class Unit : FollowAgent, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IDragHandler,
    IEndDragHandler
{
    //Stats variables
    public float health { get; set; }
    public float armor { get; set; }
    public float damage { get; set; }
    public float attackSpeed { get; set; }
    public float cost { get; set; }
    public int range { get; set; }
    public int level { get; set; }
    public int tier { get; set; }
    public string unitName { get; set; }
    public float maxHealth { get; set; }
    public bool targetable { get; set; }

    //Logic variables
    public bool spawned, selected;
    public Vector3 tilePos;

    private float timer;
    private bool hasSetTilePos;

    //Animation variables
    private Animator animator;
    private static readonly int IsIdle = Animator.StringToHash("isIdle");
    private static readonly int IsRunning = Animator.StringToHash("isRunning");

    //Attacking override variables
    public abstract Specialization specialization { get; set; }
    public abstract Specialization toAttack { get; set; }
    public abstract bool hasUpdatedBuffs { get; set; }
    public abstract bool hasAttacked { get; set; }

    public override void Start()
    {
        base.Start();
        maxHealth = health;
        hasAttacked = false;
        hasUpdatedBuffs = false;
        hasSetTilePos = false;
        timer = 0.0f;
        animator = GetComponent<Animator>();
        targetable = true;
    }

    public override void Update()
    {
        if (spawned)
        {
            if (!hasSetTilePos)
            {
                tilePos = transform.position;
                hasSetTilePos = true;
            }
        }

        if (photonView.IsMine)
        {
            base.Update();

            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out RaycastHit hit))
                {
                    if (hit.transform != transform)
                    {
                        selected = false;
                    }
                }
            }

            if (spawned)
            {
                if (health <= 0f)
                {
                    photonView.RPC("Die", RpcTarget.AllViaServer, photonView.ViewID);
                }
                else
                {
                    if (GameManager.instance.attacking)
                    {
                        moveSpeed = 8f;
                        if (target == null && EnemyCount() > 0)
                        {
                            GameManager.instance.photonView.RPC("SetRandomTarget",
                                RpcTarget.AllViaServer,
                                photonView.ViewID);
                            if (animator != null)
                            {
                                animator.SetBool("isIdle", false);
                                animator.SetBool("isRunning", true);
                            }
                        }
                        else if (target == null)
                        {
                            photonView.RPC("DoneAttacking", RpcTarget.AllViaServer, photonView.ViewID);
                        }
                        else
                        {
                            Attack();
                        }

                        timer += Time.deltaTime;
                    }

                    if (!GameManager.instance.attacking && hasAttacked)
                        hasAttacked = false;
                }
            }
        }
    }

    [PunRPC]
    public void Attack()
    {
        //Debug.Log("View ID: " + photonView.ViewID + " targeting: " + target.gameObject.GetPhotonView().ViewID);
        if (Vector3.Distance(target.position, transform.position) > range * 10f)
        {
                timer = 0.0f;
                return;
        }

        if (timer > attackSpeed && target != null)
        {
            if (animator != null)
            { 
                animator.SetBool("isRunning", false);
                animator.SetBool("isShooting", true);
            }
            try
            { GameManager.instance.photonView.RPC("LoseHealth", RpcTarget.AllViaServer, 
                    target.gameObject.GetPhotonView().ViewID, damage, photonView.ViewID);
                timer = 0.0f;
            }
            catch (NullReferenceException)
            { 
                //Debug.LogError("(Attack) Unit: " + photonView.ViewID + " could not damage targeted unit!");
            }
        }
        
    }

    [PunRPC]
    public void Die(int viewID)
    {
        GameManager.instance.viewIDstoSpawn.Add(photonView.ViewID);
        GameObject unitGO = PhotonView.Find(viewID).gameObject;
        Unit unit = unitGO.GetComponent<Unit>();
        unit.target = null;
        unit.health = unit.maxHealth;
        unit.hasAttacked = true;
        unit.targetable = false;
        unitGO.SetActive(false);
        GameManager.instance.photonView.RPC("RemoveAsTarget", RpcTarget.AllViaServer, unit.photonView.ViewID);
        GameManager.instance.photonView.RPC("RemoveFromPlayer", RpcTarget.AllViaServer, 
            viewID, Player.localPlayerInstance.playerNum);
        /*GameObject unitGO = PhotonView.Find(viewID).gameObject;
        Unit unit = unitGO.GetComponent<Unit>();
        if (unit.GetComponent<Animator>() != null)
            unit.animator.Play("Base Layer.Die");
        //PhotonNetwork.Destroy(gameObject);
        unit.target = null;
        unit.targetable = false;
        GameManager.instance.photonView.RPC("RemoveAsTarget", RpcTarget.AllViaServer, photonView.ViewID);
        unit.photonView.RPC("DoneAttacking", RpcTarget.AllViaServer, photonView.ViewID);
        unit.setDestination(transform.position);*/
    }

    [PunRPC]
    public void DoneAttacking(int viewID)
    {
        Unit unit = PhotonView.Find(viewID).GetComponent<Unit>();
        unit.target = null;
        unit.hasAttacked = true;
        unit.timer = 0;
        unit.toAttack = Specialization.Empty;
        if(unit.GetComponent<Unit>().targetable)
            unit.setDestination(tilePos);

        if (animator != null)
        {
            animator.SetBool("isIdle", true);
            animator.SetBool("isShooting", false);
        }
    }

    [PunRPC]
    public void OrderedAttack()
    {
        //Debug.Log("View ID: " + photonView.ViewID + " targeting: " + target.gameObject.GetPhotonView().ViewID);
        if (!hasAttacked)
        { 
            if (Vector3.Distance(target.position, transform.position) > range * 10f)
                return;
            
            if (timer > attackSpeed + 5.0f && target != null) 
            { 
                GameManager.instance.photonView.RPC("LoseHealth", RpcTarget.AllViaServer, 
                    target.gameObject.GetPhotonView().ViewID, damage);
                target = null;
                hasAttacked = true;
                timer = 0.0f; 
                toAttack = Specialization.Empty;
            }
        }
    }
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        //if (!photonView.IsMine)
          //  return;
        GlobalVars.shared.unitNameTip.text = unitName;
        GlobalVars.shared.unitStatsTip.text =
            $"Cost: {cost}\n\nHealth: {health}\n\nArmor: {armor}\n\nRange: {range}" +
            $"\n\nLevel: {level}\n\nTier: {tier}\nPos: " + transform.position + "\nTilePos: " + tilePos;

        if (!spawned)
        {
            //Tooltip.instance.ToggleState(true);
            //Tooltip.instance.SetUnitInfo(unitName, cost, health, armor, range, level, tier);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GlobalVars.shared.unitNameTip.text = "";
        GlobalVars.shared.unitStatsTip.text = "";
        //Tooltip.instance.ToggleState(false);
        //Tooltip.instance.ResetUnitInfo();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!selected)
        {
            //Add shader
        }
        selected = true;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if(photonView.IsMine)
            transform.position = eventData.pointerCurrentRaycast.worldPosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (spawned && photonView.IsMine)
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Camera.main.WorldToScreenPoint(transform.position)),
                out RaycastHit hit))
            {
                if (hit.transform.CompareTag("PlayerOneTile"))
                    if (hit.transform.GetComponent<Tile>().occupied == false)
                        transform.position = hit.transform.position;
                    else
                        transform.position = tilePos;
                else
                    transform.position = tilePos;
            }
            else
                transform.position = tilePos;
        }
    }

    public static int EnemyCount()
    {
        int count = 0;
        foreach (GameObject unit in GameObject.FindGameObjectsWithTag("Unit"))
        {
            if (!unit.GetPhotonView().IsMine && unit.GetComponent<Unit>().targetable)
                count++;
        }

        return count;
    }

    public static int FriendlyCount()
    {
        int count = 0;
        foreach (GameObject unit in GameObject.FindGameObjectsWithTag("Unit"))
        {
            if (unit.GetPhotonView().IsMine && unit.GetComponent<Unit>().targetable)
                count++;
        }

        return count;
    }

    public static List<GameObject> GetFriendlies()
    {
        List<GameObject> friendlies = new List<GameObject>();
        foreach (GameObject unit in GameObject.FindGameObjectsWithTag("Unit"))
        {
            if (unit.GetPhotonView().IsMine && unit.GetComponent<Unit>().targetable)
            {
                friendlies.Add(unit);
            }
        }

        return friendlies;
    }

    private bool CheckAlive(int viewID)
    {
        GameObject unit = PhotonView.Find(viewID).gameObject;
        return unit != null;
    }
}
