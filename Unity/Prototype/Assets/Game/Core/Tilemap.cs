﻿using System.Collections;
using System.Collections.Generic;
using AStar_2D;
using UnityEngine;

public class TileMap : AStarGrid
{
    public GameObject tilePrefab;
    
    private Tile[,] tiles = new Tile[8,8];
    
    void Start()
    {
        //Create tilemap
        float initX = 0, initY = 0;
        for (int i = 0; i < 8; i++)
        {
            //Imagine a 1d array as a 2d array via:
            //[i] = [(y * width=8) + x)]
            // x = i % 8, y = i / 8

            for (int j = 0; j < 8; j++)
            {
                GameObject newTile = Instantiate(tilePrefab, new Vector3(initX, 0.0f, initY),
                    Quaternion.identity);
                newTile.transform.SetParent(GameObject.Find("/TileMap").transform);
                newTile.name = "Tile " + (i + 1) + " " + (j + 1);
                GlobalVars.shared.tileList.Add(newTile);
                if (newTile.transform.position.z > 40f)
                    newTile.tag = "PlayerOneTile";
                else
                    newTile.tag = "PlayerTwoTile";
                initY += 10.05f; //Move up a row
                tiles[i, j] = newTile.GetComponent<Tile>();
            }

            initX += 10.05f; //Move up a column
            initY = 0;
        }
        
        constructGrid(tiles);
    }
}
