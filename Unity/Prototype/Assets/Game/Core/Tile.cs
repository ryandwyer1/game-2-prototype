﻿using System.Collections;
using System.Collections.Generic;
using AStar_2D;
using Photon.Pun;
using UnityEngine;

public class Tile : MonoBehaviourPun, IPathNode
{
    public bool occupied;
    
    public bool IsWalkable
    {
        get { return true; }
    }
    
    public float Weighting
    {
        get { return 0; }
    }

    public Vector3 WorldPosition
    {
        get { return transform.position; }
    }

    public PathNodeDiagonalMode DiagonalMode
    {
        get { return PathNodeDiagonalMode.DiagonalNoCutting; }
    }
}
