﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioClip battleMusic, defeatMusic, victoryMusic, introMusic, transitionSound;

    public AudioSource source;

    private static SoundManager _instance;
    IEnumerator Start()
    {
        _instance = this;
        SetMusic(4);
        yield return new WaitForSeconds(source.clip.length);
        SetMusic(0);
    }

    void Update()
    {
        source.volume += Input.GetAxis("Mouse ScrollWheel") * 1.25f;
    }

    public static void SetMusic(int musicIndex)
    {
        AudioClip toPlay;
        switch (musicIndex)
        {
            case 0:
                toPlay = _instance.battleMusic;
                break;
            case 1:
                toPlay = _instance.defeatMusic;
                break;
            case 2:
                toPlay = _instance.victoryMusic;
                break;
            case 3:
                toPlay = _instance.defeatMusic;
                break;
            case 4:
                toPlay = _instance.introMusic;
                break;
            case 5:
                toPlay = _instance.transitionSound;
                break;
            default:
                toPlay = _instance.battleMusic;
                Debug.LogError("SoundManager tried to set bad music index!");
                break;
        }
        _instance.source.clip = toPlay;
        _instance.source.Play();
    }
}
