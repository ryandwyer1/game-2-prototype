﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Object = System.Object;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Photon.Pun;

public class GlobalVars : MonoBehaviour
{
    public static GlobalVars shared;
    
    public List<GameObject> tileList, cardPrefabs, unitPrefabs, miscPrefabs;
    public List<GameObject> playerOneUnits, playerTwoUnits;
    [HideInInspector] public Hashtable tierList, specializationList;
    [HideInInspector] public List<Type> unitTypes;
    [HideInInspector] public List<GameObject> instantiatedHand;
    [HideInInspector] public Text unitNameTip, unitStatsTip;
    
    private List<List<GameObject>> prefabs;

    void Awake()
    {
        playerOneUnits = new List<GameObject>();
        playerTwoUnits = new List<GameObject>();
        instantiatedHand = new List<GameObject>();
        prefabs = new List<List<GameObject>>();
        prefabs.Add(cardPrefabs);
        prefabs.Add(unitPrefabs);
        prefabs.Add(miscPrefabs);

        string tierPath = Path.Combine(Application.streamingAssetsPath, "tierlist.json");
        string unitsPath = Path.Combine(Application.streamingAssetsPath, "unitTypes.json");
        string specializationPath = Path.Combine(Application.streamingAssetsPath, "specializations.json");
        tileList = new List<GameObject>();
        tierList = SerializedTable(tierPath);
        specializationList = SerializedTable(specializationPath);
        unitTypes = SerializedTypeList(unitsPath);
        shared = this;
    }

    void Start()
    {
        unitNameTip = GameObject.Find("Canvas/Unit Info/UnitName").GetComponent<Text>();
        unitStatsTip = GameObject.Find("Canvas/Unit Info/UnitStats").GetComponent<Text>();
    }
    
    public GameObject GetPrefab(string prefabName, bool isCard = false)
    {
        foreach (List<GameObject> prefabList in prefabs)
        {
            foreach (GameObject prefab in prefabList)
            {
                if (prefab.name == $"{prefabName}_Card" && isCard)
                    return prefab;
                else if (prefab.name == prefabName && !isCard)
                    return prefab;
            }
        }

        Debug.LogError($"Could not find prefab of name: {prefabName}");
        return null;
    }
    public int GetTier(string unitName)
    {
        foreach (string key in tierList.Keys)
        {
            if (unitName == key)
                return Int32.Parse((string)tierList[key]);
        }
        return 0;
    }

    public string Serialize(string filePath = null, string toSerialize = null, bool isArray = false)
    {
        string json = "";
        JObject jObject;

        if (toSerialize != null)
        {
            jObject = JObject.Parse(toSerialize);
            json = JsonConvert.SerializeObject(jObject);
        }
        else if (filePath != null && !isArray)
        {
            jObject = JObject.Parse(File.ReadAllText(filePath));
            json = JsonConvert.SerializeObject(jObject);
        }
        else if (filePath != null)
        {
            JArray jArray = JArray.Parse(File.ReadAllText(filePath));
            json = JsonConvert.SerializeObject(jArray);
        }
        else
            Debug.LogError("Did not have string or filePath");
        
        return json;
    }
    
    private Hashtable SerializedTable(string path)
    {
        string json = Serialize(path);
        Dictionary<string, string> tableDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
        return new Hashtable(tableDict);
    }

    private List<Type> SerializedTypeList(string path)
    {
        List<Type> types = new List<Type>();
        string json = Serialize(path, null, true);
        List<string> typeNames = JsonConvert.DeserializeObject<List<string>>(json);
        
        foreach (string name in typeNames)
        {
            Type t = Type.GetType(name);
            types.Add(t);
        }

        return types;
    }
}