﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityStandardAssets.Cameras;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviourPunCallbacks
{

    public static GameManager instance;

    public GameObject playerPrefab;
    [HideInInspector] public bool attacking, attackingDone;

    private bool playerOneDone, playerTwoDone, hasEnded;
    private float attackTimer;

    public List<int> viewIDstoSpawn;

    #region Photon Callbacks

    public override void OnLeftRoom()
    {
        SceneManager.LoadScene("Lobby");
    }

    public override void OnPlayerEnteredRoom(Photon.Realtime.Player newPlayer)
    {
        Debug.Log("OnPlayerEnteredRoom: " + newPlayer.NickName);
        if (PhotonNetwork.IsMasterClient)
            LoadArena();
    }

    public override void OnPlayerLeftRoom(Photon.Realtime.Player otherPlayer)
    {
        if (PhotonNetwork.IsMasterClient)
            LoadArena();
    }

    #endregion

    #region Unity Methods

    void Awake()
    {
        //Set up var access
        instance = this;
        GlobalVars.shared.instantiatedHand = new List<GameObject>();
    }

    void Start()
    {
        //Set volume & resolution per playerPrefs
        OptionsManager.mainAudioSource = GameObject.Find("Audio Source").GetComponent<AudioSource>();
        OptionsManager.mainAudioSource.volume = PlayerPrefs.GetFloat("Volume");

        int width = PlayerPrefs.GetInt("Width");
        int height = PlayerPrefs.GetInt("Height");
        GameObject.Find("Canvas").GetComponent<CanvasScaler>().referenceResolution = new Vector2(width, height);

        attackTimer = 0.0f;
        //Instantiate players
        int secondPlayerViewID = 0;
        if (Player.localPlayerInstance == null)
        {
            Vector3 pos = new Vector3(35f, 35f, 105f);
            GameObject localPlayer = PhotonNetwork.Instantiate(playerPrefab.name, pos, Quaternion.identity);
            if (PhotonNetwork.PlayerList.Length > 1)
            {
                secondPlayerViewID = localPlayer.GetPhotonView().ViewID;
                localPlayer.transform.position = new Vector3(35f, 35f, -30f);
                GameObject.Find("/Camera").transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                photonView.RPC("UpdatePlayerNums", RpcTarget.AllViaServer, secondPlayerViewID);
                localPlayer.GetComponent<Player>().playerNum = 2;
            }

            GameObject.Find("/Camera").GetComponent<FreeLookCam>().SetTarget(localPlayer.transform);
        }

        SpawnStore(GenerateCards());
    }

    void Update()
    {
        if (attacking && PhotonNetwork.IsMasterClient)
        {
            CheckAttacks();
            attackTimer += Time.deltaTime;
        }
    }

    #endregion

    #region Public Methods

    private Specialization GetUnitSpecialization(string unitName)
    {
        foreach (DictionaryEntry pair in GlobalVars.shared.specializationList)
        {
            if (pair.Key.ToString() == unitName)
            {
                Enum.TryParse(pair.Value.ToString(), out Specialization result);
                return result;
            }
        }

        return Specialization.Empty;
    }

    public void EndTurnButton()
    {
        photonView.RPC("EndTurn", RpcTarget.AllViaServer, Player.localPlayerInstance.playerNum);
    }

    [PunRPC]
    public void AddToPlayer(int viewID, int playerNum)
    {
        switch (playerNum)
        {
            case 1:
                GlobalVars.shared.playerOneUnits.Add(PhotonView.Find(viewID).gameObject);
                break;
            case 2:
                GlobalVars.shared.playerTwoUnits.Add(PhotonView.Find(viewID).gameObject);
                break;
            default:
                Debug.LogError("Error at AddToPlayer playerNum");
                break;

        }
    }

    [PunRPC]
    public void RemoveFromPlayer(int viewID, int playerNum)
    {
        switch (playerNum)
        {
            case 1:
                GlobalVars.shared.playerOneUnits.Remove(PhotonView.Find(viewID).gameObject);
                break;
            case 2:
                GlobalVars.shared.playerTwoUnits.Remove(PhotonView.Find(viewID).gameObject);
                break;
            default:
                Debug.LogError("Tried to remove from player past two");
                break;
        }
    }

    [PunRPC]
    public void EndTurn(int playerNum)
    {
        hasEnded = false;
        switch (playerNum)
        {
            case 1:
                playerOneDone = true;
                break;
            case 2:
                playerTwoDone = true;
                break;
            default:
                //Debug.LogError("Tried to end turn past two players!");
                break;
        }

        if (playerOneDone && playerTwoDone)
        {
            if (!attackingDone && !attacking)
            {
                Debug.LogError("Turn done, attacking");
                attacking = true;
            }
        }
    }

    [PunRPC]
    public void LoseHealth(int viewID, float damage, int damagerID)
    {
        try
        {
            GameObject unit = PhotonView.Find(viewID).gameObject;

            float damageMult;
            if (SpecializationManager.instance.antiTank)
            {
                damageMult = 1f - (0.026f * unit.GetComponent<Unit>().armor);
            }
            else
            {
                damageMult = 1f - (0.052f * unit.GetComponent<Unit>().armor);
            }

            if (damageMult < 0)
                damageMult = 0f;

            unit.GetComponent<Unit>().health -= (damage * damageMult);

            if (Unit.EnemyCount() > 0 && unit.GetComponent<Unit>().health <= 0f)
            {
                photonView.RPC("SetRandomTarget", RpcTarget.AllViaServer,
                    damagerID);
            }
            else if (Unit.EnemyCount() <= 0)
                PhotonView.Find(damagerID).RPC("DoneAttacking", RpcTarget.AllViaServer, damagerID);
        }
        catch (NullReferenceException)
        {
            //TODO: This
            //Debug.LogError("(LoseHealth) Unit " + viewID + " could not be damaged!");
        }
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    [PunRPC]
    public void UpdatePlayerNums(int secondPlayerViewID)
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            if (player.GetPhotonView().ViewID == secondPlayerViewID)
                player.GetComponent<Player>().playerNum = 2;
            else
                player.GetComponent<Player>().playerNum = 1;
        }
    }

    [PunRPC]
    public void UpdateOwnerSpecializations()
    {
        Array.Clear(Player.localPlayerInstance.specializations, 0, Player.localPlayerInstance.specializations.Length);
        foreach (GameObject unit in GameObject.FindGameObjectsWithTag("Unit"))
        {
            if (unit.GetPhotonView().IsMine)
            {
                Player.localPlayerInstance.specializations[(int) unit.GetComponent<Unit>().specialization]++;
            }
            else
            {
                if (PhotonNetwork.PlayerList.Length < 2)
                    return;

                GameObject otherPlayer = null;
                GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
                foreach (GameObject player in players)
                {
                    if (player.GetComponent<Player>().playerNum != Player.localPlayerInstance.playerNum)
                    {
                        otherPlayer = player;
                        foreach (DictionaryEntry pair in GlobalVars.shared.specializationList)
                        {
                            string unitName = pair.Key.ToString();
                            Enum.TryParse(pair.Value.ToString(), out Specialization unitSpec);

                            if (unitName == unit.name.Split('_')[0])
                                otherPlayer.GetComponent<Player>().specializations[(int) unitSpec]++;
                        }

                        player.GetComponent<Player>().specializations[(int) unit.GetComponent<Unit>().specialization]++;
                    }
                }
            }
        }

        Player.localPlayerInstance.UpdateBuffs();
        //Orders.instance.UpdatePanels();
        //Orders.instance.GetEnemyTargets();
    }

    [PunRPC]
    public void SpawnUnit(Vector3 newPos, int unitViewID)
    {
        GameObject unit = PhotonView.Find(unitViewID).gameObject;
        unit.transform.SetParent(GameObject.Find("/Units").transform);
        unit.transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        if (Player.localPlayerInstance.playerNum == 2)
            unit.transform.rotation = Quaternion.Euler(0f, 0f, 0f);

        instance.photonView.RPC("AddToPlayer", RpcTarget.All,
            unit.GetPhotonView().ViewID, Player.localPlayerInstance.playerNum);


        foreach (var type in GlobalVars.shared.unitTypes.Where(type =>
            type.ToString() == unit.name.Split('_')[0]))
        {
            unit.AddComponent(type);
        }

        Unit unitScript = unit.GetComponent<Unit>();
        unitScript.spawned = true;

        if (GetUnitSpecialization(unitScript.unitName) == Specialization.Air)
        {
            unit.transform.position = new Vector3(unit.transform.position.x, unit.transform.position.y + 7.0f,
                unit.transform.position.z);
            unitScript.tilePos.y += 7.0f;
        }
    }

    [PunRPC]
    public void SetUnitTargets(int unitID, int toAttack)
    {
        List<GameObject> targets = new List<GameObject>();
        foreach (GameObject enemyUnit in GameObject.FindGameObjectsWithTag("Unit"))
        {
            if (!enemyUnit.GetPhotonView().IsMine && (int) enemyUnit.GetComponent<Unit>().specialization == toAttack)
                targets.Add(enemyUnit);
        }

        if (targets.Count <= 0 || toAttack == 10)
            photonView.RPC("SetRandomTarget", RpcTarget.AllViaServer, unitID);
        else
        {
            GameObject unit = PhotonView.Find(unitID).gameObject;
            unit.GetComponent<Unit>().target = targets[Random.Range(0, targets.Count)].transform;
        }
    }

    [PunRPC]
    public void SetRandomTarget(int viewID)
    {
        List<GameObject> targets = new List<GameObject>();
        foreach (GameObject enemyUnit in GameObject.FindGameObjectsWithTag("Unit"))
        {
            if (!enemyUnit.GetPhotonView().IsMine && enemyUnit.GetComponent<Unit>().targetable)
                targets.Add(enemyUnit);
        }

        Unit unit = null;
        try
        {
            unit = PhotonView.Find(viewID).gameObject.GetComponent<Unit>();
        }
        catch (NullReferenceException)
        {
            return;
        }

        if (targets.Count <= 0 && Unit.EnemyCount() > 0)
        {
            Debug.LogError("Unit: " + viewID + " could not find a target!");
            unit.setDestination(unit.tilePos);
            return;
        }


        unit.toAttack = Specialization.Empty;

        try
        {
            unit.target = targets[Random.Range(0, targets.Count)].transform;
        }
        catch (ArgumentOutOfRangeException)
        {
            unit.target = null;
            unit.setDestination(unit.tilePos);
        }

        //TODO: ArgumentOutOfBoundsException??? Why???????
    }

    [PunRPC]
    public void RemoveAsTarget(int viewID)
    {
        foreach (GameObject unit in GameObject.FindGameObjectsWithTag("Unit"))
        {
            if(unit.GetComponent<Unit>().target != null)
                if (unit.GetComponent<Unit>().target.gameObject.GetPhotonView().ViewID == viewID)
                    unit.GetComponent<Unit>().target = null;
        }
    }

    [PunRPC]
    public void EndLevel()
    {
        if (!hasEnded)
        {
            Debug.Log("Turn done, ending turn");

            attacking = attackingDone = false;
            
            for (int i = 0; i < viewIDstoSpawn.Count; i++)
            {
                GameObject unit = PhotonView.Find(viewIDstoSpawn[i]).gameObject;
                unit.gameObject.SetActive(true);
                instance.photonView.RPC("AddToPlayer", RpcTarget.All,
                    unit.GetPhotonView().ViewID, Player.localPlayerInstance.playerNum);
            }
            
            foreach (GameObject unit in GameObject.FindGameObjectsWithTag("Unit"))
            {
                unit.GetComponent<Unit>().toAttack = Specialization.Empty;
                unit.GetComponent<Unit>().target = null;
                unit.GetComponent<Unit>().targetable = true;
            }

            foreach (GameObject card in GlobalVars.shared.instantiatedHand)
            {
                PhotonNetwork.Destroy(card);
            }

            GlobalVars.shared.instantiatedHand.Clear();

            SpawnStore(GenerateCards());
            Player.localPlayerInstance.money += 500f;
            GameObject.Find("Canvas/Player Stats/Money").GetComponent<Text>().text =
                $"Money: {Player.localPlayerInstance.money}";
            playerOneDone = playerTwoDone = false;
            hasEnded = true;
            if (Unit.FriendlyCount() <= 0)
                Player.localPlayerInstance.life--;

            if (Player.localPlayerInstance.life <= 0)
                photonView.RPC("EndGame", RpcTarget.AllViaServer, Player.localPlayerInstance.playerNum);

            foreach (GameObject unit in GameObject.FindGameObjectsWithTag("Unit"))
            {
                unit.GetComponent<Unit>().setDestination(unit.GetComponent<Unit>().tilePos);
            }
        }
    }

    [PunRPC]
    public void EndGame(int loser)
    {
        SceneManager.LoadScene(0);
    }
    
    

    [PunRPC]
    public void CheckAttacks()
    {
        if (Unit.EnemyCount() > 0 && Unit.FriendlyCount() > 0)
            return;

        attacking = false;
        attackingDone = true;
        attackTimer = 0f;
        photonView.RPC("EndLevel", RpcTarget.AllViaServer);
    }

    #endregion

    #region Private Methods

    private List<GameObject> GenerateCards()
    {
        List<GameObject> cards = new List<GameObject>();
        string[] keys = new string[GlobalVars.shared.tierList.Count];
        GlobalVars.shared.tierList.Keys.CopyTo(keys, 0);

        int foundCards = 0, tierOne = 0, tierTwo = 0, tierThree = 0;
        while (foundCards < 9)
        {
            while (tierOne < 4)
            {
                string key = keys[Random.Range(0, keys.Length)];
                if (GlobalVars.shared.tierList[key].Equals("1"))
                {
                    //Create card of unit "key" and put it in bunker
                    GameObject unit = GlobalVars.shared.GetPrefab(key, true);
                    tierOne++;
                    foundCards++;
                    cards.Add(unit);
                }
            }

            while (tierTwo < 3)
            {
                string key = keys[Random.Range(0, keys.Length)];
                if (GlobalVars.shared.tierList[key].Equals("2"))
                {
                    GameObject unit = GlobalVars.shared.GetPrefab(key, true);
                    tierTwo++;
                    foundCards++;
                    cards.Add(unit);
                }
            }

            while (tierThree < 2)
            {
                string key = keys[Random.Range(0, keys.Length)];
                if (GlobalVars.shared.tierList[key].Equals("3"))
                {
                    GameObject unit = GlobalVars.shared.GetPrefab(key, true);
                    tierThree++;
                    foundCards++;
                    cards.Add(unit);
                }
            }
        }

        return cards;
    }

    private void SpawnStore(List<GameObject> hand)
    {
        float xPos = -75f, yPos = 195f;
        for (int i = 0; i < hand.Count; i++)
        {
            if (i % 3 == 0 && i > 0)
            {
                xPos = -75f;
                yPos -= 150f;
            }

            GameObject card = hand[i];
            Vector3 spawnPos = new Vector3(xPos, yPos, 0f);
            GameObject newCard = PhotonNetwork.Instantiate(card.name, spawnPos, Quaternion.identity);
            newCard.transform.SetParent(GameObject.Find("Canvas/Store/Units").transform);
            newCard.GetComponent<RectTransform>().anchoredPosition = spawnPos;
            //string[] cardName = card.name.Split('_');
            //newCard.name = cardName[0] + " Card";
            GlobalVars.shared.instantiatedHand.Add(newCard);
            xPos += 115;
        }
    }

    private void LoadArena()
    {
        if (!PhotonNetwork.IsMasterClient)
            Debug.LogError("PhotonNetwork: Trying to load level but are not master client!");

        Debug.Log("PhotonNetwork: Load Level - " + PhotonNetwork.CurrentRoom);
        PhotonNetwork.LoadLevel("Test");
    }
    
    private GameObject GetOtherPlayer(int playerNum)
    {
        if (PhotonNetwork.PlayerList.Length < 2)
            Debug.LogError("Tried to get other player w/ only one player!");
        
        

        return null;
    }

    #endregion
}