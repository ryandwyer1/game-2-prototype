﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Photon.Pun;
using UnityEngine;

public class Player : MonoBehaviourPun
{
    #region Public Attributes
    public int life = 4;
    public float money = 500f;
    public int playerNum = 1;
    public static Player localPlayerInstance;
    #endregion
    
    #region Fields
    [HideInInspector] public int[] specializations;
    public static readonly int[] requirements = {4, 3, 3, 2, 2, 2, 3, 2, 3, 2};

    private SpecializationManager specializationManager;
    #endregion
    

    void Awake()
    {
        specializationManager = new SpecializationManager();
        if (photonView.IsMine)
        {
            localPlayerInstance = this;
        }
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        specializations = new int[10];
    }

    void Update()
    {
        if (life <= 0)
        {
            PhotonNetwork.Destroy(gameObject);
            GameManager.instance.LeaveRoom();
        }
    }
    
    #region Public Methods

    public void UpdateBuffs()
    {
        for (int specialization = 0; specialization < 10; specialization++)
        {
            if (specializations[specialization] > requirements[specialization])
            {
                specializationManager.ActivateBuff((Specialization)specialization);
            }
        }
    }
    #endregion
    
    #region Private Methods
    #endregion
}
