﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class Lobby : MonoBehaviourPunCallbacks
{
    public AudioSource audioSource;
    public AudioClip musicClip, transitionClip;
    
    [SerializeField] private byte maxPlayers = 8;
    [SerializeField] private GameObject controlPanel;
    [SerializeField] private GameObject progressLabel;
    
    private string gameVersion = "1";
    private bool isConnecting = false;

    void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    void Start()
    {
        audioSource.clip = musicClip;
        audioSource.Play();
        progressLabel.SetActive(false);
        controlPanel.SetActive(true);
    }
    
    void Update()
    {
        //audioSource.volume += Input.GetAxis("Mouse ScrollWheel") * 1.25f;
    }

    public void Connect()
    {
        progressLabel.SetActive(true);
        controlPanel.SetActive(false);
        if (PhotonNetwork.IsConnected)
            PhotonNetwork.JoinRandomRoom();
        else
        {
            isConnecting = PhotonNetwork.ConnectUsingSettings();
            PhotonNetwork.GameVersion = gameVersion;
            audioSource.clip = transitionClip;
            audioSource.Play();
        }
    }
    
    #region PUN Callbacks

    public override void OnConnectedToMaster()
    {
        if(isConnecting)
        {
            Debug.Log("OnConnectedToMaster() was called by PUN");
            PhotonNetwork.JoinRandomRoom();
            isConnecting = false;
        }
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("No room was available, creating one");
        PhotonNetwork.CreateRoom(null, new RoomOptions());
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom called by PUN");
        //if (PhotonNetwork.CurrentRoom.PlayerCount >= 2 && PhotonNetwork.CurrentRoom.PlayerCount < 8)
            PhotonNetwork.LoadLevel("Test");
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        progressLabel.SetActive(false);
        controlPanel.SetActive(true);
        isConnecting = false;
        Debug.LogWarning("OnDisconnected called for reason: " + cause);
    }

    #endregion
}
