﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsManager : MonoBehaviour
{
    public static AudioSource mainAudioSource;

    public AudioSource startingSource;
    public GameObject optionsMenu;
    public CanvasScaler canvasScaler;
    
    #region Unity Methods
    void Awake()
    {
        optionsMenu.SetActive(false);
        DontDestroyOnLoad(gameObject);
        PlayerPrefs.SetFloat("Volume", 50f);
        PlayerPrefs.SetInt("Width", Screen.currentResolution.width);
        PlayerPrefs.SetInt("Height", Screen.currentResolution.height);
    }

    void Start()
    {
        mainAudioSource = startingSource;
    }
    #endregion
    
    #region Public Methods

    public void SettingsButton()
    {
        if (!optionsMenu.activeSelf)
        {
            foreach (Transform child in GameObject.Find("Canvas").transform)
            {
                if (!child.CompareTag("Settings"))
                    child.gameObject.SetActive(false);
                else
                    child.gameObject.SetActive(true);
            }
        }
        else
        {
            foreach (Transform child in GameObject.Find("Canvas").transform)
            {
                child.gameObject.SetActive(true);
            }
            optionsMenu.SetActive(false);
        }
    }

    public void ScreenMode(int index)
    {
        switch (index)
        {
            case 0:
                Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
                Screen.fullScreen = true;
                break;
            case 1:
                Screen.fullScreenMode = FullScreenMode.FullScreenWindow;
                Screen.fullScreen = true;
                break;
            case 2:
                Screen.fullScreen = false;
                break;
            default:
                Debug.LogError("Screenmode set outside index");
                break;
        }
    }
    
    public void VolumeSlider(float slider)
    {
        startingSource.volume = slider;
        PlayerPrefs.SetFloat("Volume", slider);
    }

    public void Resolution(int index)
    {
        switch (index)
        {
            case 0: //1280x720
                Screen.SetResolution(1280, 720, FullScreenMode.Windowed);
                canvasScaler.referenceResolution = new Vector2(1280f, 720f);
                PlayerPrefs.SetInt("Width", 1280);
                PlayerPrefs.SetInt("Height", 720); 
                break;
            case 1: //1600x900
                Screen.SetResolution(1600, 900, FullScreenMode.Windowed);
                canvasScaler.referenceResolution = new Vector2(1600f, 900f);
                PlayerPrefs.SetInt("Width", 1600);
                PlayerPrefs.SetInt("Height", 900);
                break;
            case 2: //1920x1080
                Screen.SetResolution(1920, 1080, FullScreenMode.Windowed);
                canvasScaler.referenceResolution = new Vector2(1920f, 1080f);
                PlayerPrefs.SetInt("Width", 1920);
                PlayerPrefs.SetInt("Height", 1080);
                break;
            case 3: //2560x1440
                Screen.SetResolution(2560, 1440, FullScreenMode.Windowed);
                canvasScaler.referenceResolution = new Vector2(2560f, 1440f);
                PlayerPrefs.SetInt("Width", 2560);
                PlayerPrefs.SetInt("Height", 1440);
                break;
            default:
                Debug.LogError("Resolution set outside index");
                break;
        }
    }
    #endregion
}
